# io-oidis-xcpplint v2022.2.0

> Lint tool for Cpp projects, which requires Oidis Framework coding standards

## Requirements

This library does not have any special requirements but it depends on the 
[Oidis Builder](https://gitlab.com/oidis/io-oidis-builder). See the Builder requirements before you build this project.

## Project build

The project build is fully automated. For more information about the project build, see the 
[Oidis Builder](https://gitlab.com/oidis/io-oidis-builder) documentation.

## Documentation

This project provides automatically-generated documentation in [TypeDoc](http://typedoc.org/) from the TypeScript source by running the 
`wui docs` command from the {projectRoot} folder.

> NOTE: The documentation is accessible also from the {projectRoot}/build/target/docs/index.html file after a successful creation.

## History

### v2022.2.0, v2022.1.0
Just reflect changes in dependencies.
### v2022.0.0
Just reflect changes in dependencies.
### v2021.2.0
Fixed bug connected with IntelliJ indexing after IDEA update.
### v2020.1.0
Just reflect changes in dependencies.
### v2020.0.0
Identity update. Change of configuration files format.
### v2019.3.0
Refactoring of namespaces. Migration to gitlab. Initial update of identity for project fork.
### v2019.1.0
Fixed indenting.
### v2019.0.2
Updated test resources configuration. Fixed handling of negative lookbehind syntax at unsupported environments. 
Fixed extern C formatting. Usage of new app loader.
### v2019.0.1
Added documentation. Fixed false positives and parser bugs. Added more tests.
### v2019.0.0
Added ability to use rules configuration. Added autofix ability for another set of rules. Added sorting validation and fix. 
Fixed several parser issues. Added more tests.
### v2018.3.0
Refactoring of CPP parser and project structure. Added implementation of linting rules and several autofix methods. Update of WUI Core. 
### v2018.0.0
Update of WUI Core. Added basic Life-Cycle environment. Added basic Rules implementation.
### v1.1.1
Added regEx-based code blocks classifier.
### v1.1.0
Added initial version of source code parser.
### v1.0.0
Initial release.

## License

This software is owned or controlled by Oidis. 
The use of this software is governed by the BSD-3-Clause Licence distributed with this material.
  
See the `LICENSE.txt` file for more details.

---

Author Jakub Cieslar, 
Copyright 2017-2019 [NXP](http://nxp.com/),
Copyright 2019-2022 [Oidis](https://www.oidis.org/)
