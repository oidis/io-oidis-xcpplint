/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/// <reference path="../../../../reference.d.ts" />
namespace Io.Oidis.XCppLint.Enums {
    "use strict";
    import BaseEnum = Io.Oidis.Commons.Primitives.BaseEnum;

    export class TokenType extends BaseEnum {
        public static readonly COMMENT : string = ":comment:";
        public static readonly DIRECTIVE : string = ":directive:";
        public static readonly BLOCK : string = ":block:";
    }
}
