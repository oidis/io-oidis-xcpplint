/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/// <reference path="../../../../reference.d.ts" />
namespace Io.Oidis.XCppLint.Enums {
    "use strict";
    import BaseEnum = Io.Oidis.Commons.Primitives.BaseEnum;

    export class SpecType extends BaseEnum {
        public static readonly PUBLIC : string = "public";
        public static readonly PRIVATE : string = "private";
        public static readonly PROTECTED : string = "protected";
    }
}
