/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/// <reference path="../../../../../reference.d.ts" />
namespace Io.Oidis.XCppLint.Rules.Before {
    "use strict";
    import BaseBefore = Io.Oidis.XCppLint.Structures.BaseBefore;
    import BaseProcessingInfo = Io.Oidis.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import ParensProcessingInfo = Io.Oidis.XCppLint.Structures.ProcessingInfo.ParensProcessingInfo;

    export class ParensBefore extends BaseBefore {
        private static instance : ParensBefore;
        private processingInfo : ParensProcessingInfo;

        public static getInstance() : BaseBefore {
            if (!ParensBefore.instance) {
                ParensBefore.instance = new ParensBefore();
            }
            return ParensBefore.instance;
        }

        constructor() {
            super();
            this.processingInfo = new ParensProcessingInfo();
        }

        public Reset() : void {
            super.Reset();
            this.processingInfo.Clear();
        }

        public getResults($processingInfo : BaseProcessingInfo) : ParensProcessingInfo {
            this.run($processingInfo);
            return this.processingInfo;
        }

        private run($processingInfo : BaseProcessingInfo) : void {
            if (!this.Done()) {
                const regexs : RegExp[] = [
                    /\bif\s*\((.*)\)\s*{/g,
                    /\bfor\s*\((.*)\)\s*{/g,
                    /\bwhile\s*\((.*)\)\s*{/g,
                    /\bswitch\s*\((.*)\)\s*{/g
                ];

                let fncall : string = $processingInfo.Child().body;

                for (const pattern of regexs) {
                    const match = pattern.exec($processingInfo.Child().body);
                    if (match != null) {
                        fncall = match[1];
                    }
                }

                this.processingInfo.Fncall(fncall);

                if (!/\b(if|for|while|switch|return|new|delete|catch|sizeof)\b/.test(fncall) &&
                    !/ \([^)]+\)\([^)]*(\)|,$)/.test(fncall) &&
                    !/ \([^)]+\)\[[^\]]+\]/.test(fncall)) {
                    this.processingInfo.CommonCondition(true);
                }

                this.Done(true);
            }
        }
    }
}
