/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/// <reference path="../../../../../reference.d.ts" />
namespace Io.Oidis.XCppLint.Rules.Before {
    "use strict";
    import BaseBefore = Io.Oidis.XCppLint.Structures.BaseBefore;
    import BaseProcessingInfo = Io.Oidis.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Io.Oidis.Commons.Utils.StringUtils;
    import TemplatedVarsProcessingInfo = Io.Oidis.XCppLint.Structures.ProcessingInfo.TemplatedVarsProcessingInfo;
    import LogIt = Io.Oidis.Commons.Utils.LogIt;

    export class TemplatedVarsBefore extends BaseBefore {
        private static instance : TemplatedVarsBefore;
        private processingInfo : TemplatedVarsProcessingInfo;

        public static getInstance() : BaseBefore {
            if (!TemplatedVarsBefore.instance) {
                TemplatedVarsBefore.instance = new TemplatedVarsBefore();
            }
            return TemplatedVarsBefore.instance;
        }

        constructor() {
            super();
            this.processingInfo = new TemplatedVarsProcessingInfo();
        }

        public Reset() : void {
            super.Reset();
        }

        public getResults($processingInfo : BaseProcessingInfo) : TemplatedVarsProcessingInfo {
            this.run($processingInfo);
            return this.processingInfo;
        }

        private run($processingInfo : BaseProcessingInfo) : void {
            if (!this.Done()) {
                if (StringUtils.Contains($processingInfo.Child().body, "template")) {
                    this.processingInfo.Matches(null);
                    return;
                }

                const regex : RegExp = new RegExp("\\w*(\\s*<\\s*[^>]*\\s*(?<!-)>\\s*)(.)", "gm");
                const matches : RegExpExecArray = regex.exec($processingInfo.Child().body);

                if (!matches || (StringUtils.EndsWith(matches[1], ">") && matches[2] === ">")) {
                    this.processingInfo.Matches(null);
                } else {
                    this.processingInfo.Matches(matches);
                }

                this.Done(true);
            }
        }
    }
}
