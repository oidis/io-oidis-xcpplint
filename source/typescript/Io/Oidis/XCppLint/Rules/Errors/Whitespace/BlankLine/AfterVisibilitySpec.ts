/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/// <reference path="../../../../../../../reference.d.ts" />
namespace Io.Oidis.XCppLint.Rules.Errors.Whitespace.BlankLine {
    "use strict";
    import LogIt = Io.Oidis.Commons.Utils.LogIt;
    import BaseLintRule = Io.Oidis.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Io.Oidis.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Io.Oidis.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Io.Oidis.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Io.Oidis.Commons.Utils.StringUtils;
    import IToken = Io.Oidis.XCppLint.Interfaces.IToken;
    import TokenType = Io.Oidis.XCppLint.Enums.TokenType;

    export class AfterVisibilitySpec extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.WHITESPACE.BLANK_LINE.AFTER_VISIBILITY_SPEC);
            this.setSensitivity(TokenType.COMMENT, TokenType.BLOCK);
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            const prevChild : IToken = $processingInfo.PrevChild();
            if (prevChild &&
                (StringUtils.EndsWith(prevChild.body, "public:") ||
                    StringUtils.EndsWith(prevChild.body, "protected:") ||
                    StringUtils.EndsWith(prevChild.body, "private:"))) {
                if (StringUtils.StartsWith($processingInfo.Child().body, "\n\n")) {
                    $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                }
            }
        }
    }
}
