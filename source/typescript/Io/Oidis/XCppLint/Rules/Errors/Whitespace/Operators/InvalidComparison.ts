/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/// <reference path="../../../../../../../reference.d.ts" />
namespace Io.Oidis.XCppLint.Rules.Errors.Whitespace.Operators {
    "use strict";
    import LogIt = Io.Oidis.Commons.Utils.LogIt;
    import BaseLintRule = Io.Oidis.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Io.Oidis.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Io.Oidis.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Io.Oidis.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;

    export class InvalidComparison extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.WHITESPACE.OPERATORS.INVALID_COMPARISON);
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            if (/(>=|<=|==|!=|&=|\^=|\|=|\+=|\*=|\/=|\%=)/.test($processingInfo.Child().body) &&
                !/ {1}(>=|<=|==|!=|&=|\^=|\|=|\+=|\*=|\/=|\%=) {1}/.test($processingInfo.Child().body)) {
                $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
            }
            if ((/ \([^)].*(<|>).*\) /.test($processingInfo.Child().body) || /=.*([^-]>|<).*/gm.test($processingInfo.Child().body)) &&
                !this.isCppCast($processingInfo.Child()) &&
                !/(<<|>>)/.test($processingInfo.Child().body) &&
                !/(>=|<=)/.test($processingInfo.Child().body) &&
                !/(->)/.test($processingInfo.Child().body) &&
                !/ {1}(<|>) {1}/.test($processingInfo.Child().body) &&
                !new RegExp("\\w*(?<!template)(\\s*<\\s*.*\\s*>\\s*)(.)", "gm").test($processingInfo.Child().body)) {
                $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
            }
        }
    }
}
