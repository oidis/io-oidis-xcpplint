/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/// <reference path="../../../../../../../reference.d.ts" />
namespace Io.Oidis.XCppLint.Rules.Errors.Whitespace.Newline {
    "use strict";
    import BaseLintRule = Io.Oidis.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Io.Oidis.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Io.Oidis.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Io.Oidis.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Io.Oidis.Commons.Utils.StringUtils;
    import TokenType = Io.Oidis.XCppLint.Enums.TokenType;

    export class MissingAfterComment extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.WHITESPACE.NEWLINE.MISSING_AFTER_COMMENT);
            this.setSensitivity(TokenType.COMMENT);
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            if (!StringUtils.EndsWith($processingInfo.Child().body, "\n") &&
                !StringUtils.StartsWith($processingInfo.NextToken().body, "\n")) {

                // ignore comments as unused parameters
                if ($processingInfo.NextToken() && $processingInfo.NextToken().line === $processingInfo.Child().line &&
                    $processingInfo.PrevChild() && $processingInfo.PrevChild().type === TokenType.BLOCK) {
                    return;
                }

                $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
            }
        }

        public Fix($pinfo : BaseProcessingInfo) : boolean {
            if (!StringUtils.EndsWith($pinfo.Child().body, "\n")) {
                if (!($pinfo.NextToken() && StringUtils.StartsWith($pinfo.NextToken().body, "\n"))) {
                    $pinfo.Child().body += "\n";
                }
            }

            return true;
        }
    }
}
