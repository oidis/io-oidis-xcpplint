/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/// <reference path="../../../../../../../reference.d.ts" />
namespace Io.Oidis.XCppLint.Rules.Errors.Whitespace.BlankLine {
    "use strict";
    import LogIt = Io.Oidis.Commons.Utils.LogIt;
    import BaseLintRule = Io.Oidis.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Io.Oidis.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Io.Oidis.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Io.Oidis.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Io.Oidis.Commons.Utils.StringUtils;
    import TokenType = Io.Oidis.XCppLint.Enums.TokenType;

    export class Preceded extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.WHITESPACE.BLANK_LINE.PRECEDED);
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            if (/(public|private|protected):/g.test($processingInfo.Child().body)) {
                if (!/(\n{2} {1,})(public|private|protected):/.test($processingInfo.Child().body)) {
                    if ($processingInfo.Child().childIndex !== 0) {
                        if ($processingInfo.PrevToken() && $processingInfo.PrevToken().type === TokenType.DIRECTIVE &&
                            StringUtils.EndsWith($processingInfo.PrevToken().body, "\n\n")) {
                            return;
                        }
                        $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                    }
                }
            }
        }

        public Fix($pinfo : BaseProcessingInfo) : boolean {
            $pinfo.Child().body = "\n" + $pinfo.Child().body;
            return true;
        }
    }
}
