/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/// <reference path="../../../../../../../../reference.d.ts" />
namespace Io.Oidis.XCppLint.Rules.Errors.Whitespace.Wui.TemplatedVars {
    "use strict";
    import IErrorReporterObject = Io.Oidis.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Io.Oidis.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Io.Oidis.Commons.Utils.StringUtils;
    import BaseLintRule = Io.Oidis.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Io.Oidis.XCppLint.Core.ERRORS;
    import TemplatedVarsBefore = Io.Oidis.XCppLint.Rules.Before.TemplatedVarsBefore;
    import TemplatedVarsProcessingInfo = Io.Oidis.XCppLint.Structures.ProcessingInfo.TemplatedVarsProcessingInfo;

    export class InnerSpaces extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.WHITESPACE.WUI.TEMPLATED_VARS.INNER_SPACES);
            this.setOnBefore(TemplatedVarsBefore.getInstance());
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            if (this.isCppCast($processingInfo.Child())) {
                return;
            }

            const pInfo : TemplatedVarsProcessingInfo = <TemplatedVarsProcessingInfo>this.getOnBefore().getResults($processingInfo);

            if (pInfo.Matches() && (StringUtils.Contains(pInfo.Matches()[1], "< ") ||
                StringUtils.Contains(pInfo.Matches()[1], " >"))) {
                if (!StringUtils.Contains($processingInfo.Child().body, pInfo.Matches()[0])) {
                    return;
                }

                $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
            }
        }
    }
}
