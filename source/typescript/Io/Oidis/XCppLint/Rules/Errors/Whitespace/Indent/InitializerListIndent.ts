/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/// <reference path="../../../../../../../reference.d.ts" />
namespace Io.Oidis.XCppLint.Rules.Errors.Whitespace.Indent {
    "use strict";
    import LogIt = Io.Oidis.Commons.Utils.LogIt;
    import BaseLintRule = Io.Oidis.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Io.Oidis.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Io.Oidis.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Io.Oidis.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Io.Oidis.Commons.Utils.StringUtils;
    import Config = Io.Oidis.XCppLint.Core.Config;

    export class InitializerListIndent extends BaseLintRule {

        private expectedIndentSize : number;

        constructor() {
            super();
            this.setErrorObject(ERRORS.WHITESPACE.INDENT.INITIALIZER_LIST_INDENT);
            this.expectedIndentSize = 4;
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            const nesting : number = $processingInfo.Child().nesting;

            if (BaseLintRule.isCtor($processingInfo.Child().body)) {
                if (/\)\s*:\s*/m.test($processingInfo.Child().body)) {  // check if ctor contains initializer list
                    const re : RegExp = new RegExp(
                        "(\\r\\n|\\n) {" + ((this.expectedIndentSize * nesting) + (2 * this.expectedIndentSize)) + "}: ",
                        "m");
                    if (!re.test($processingInfo.Child().body)) {    // check if colon is correctly formatted
                        $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                    }
                }
            }
        }

        public Fix($pinfo : BaseProcessingInfo) : boolean {
            const firstEndBrace : number = StringUtils.IndexOf($pinfo.Child().body, ")");
            const originalInitList : string = StringUtils.Substring($pinfo.Child().body, firstEndBrace + 1);
            const elems : string[] = StringUtils.Split(originalInitList, ",");

            const nesting : number = $pinfo.Child().nesting;

            for (let i = 0; i < elems.length; i++) {
                elems[i] = elems[i].trim();
                if (StringUtils.StartsWith(elems[i], ":")) {
                    elems[i] = "\n"
                        + this.generateSpaces((this.expectedIndentSize * nesting) + (2 * this.expectedIndentSize))
                        + elems[i];
                } else {
                    elems[i] = "\n"
                        + this.generateSpaces((this.expectedIndentSize * nesting) + (2 * this.expectedIndentSize) + 2)
                        + elems[i];
                }
            }

            const fixedList : string = elems.join();

            $pinfo.Child().body = StringUtils.Replace($pinfo.Child().body, originalInitList, fixedList);

            return true;
        }

        public LoadConfig($config : Config) : void {
            const temp : number = (<number>$config.getPropertyValue("spaces"));
            if (temp) {
                this.expectedIndentSize = temp;
            }
        }
    }
}
