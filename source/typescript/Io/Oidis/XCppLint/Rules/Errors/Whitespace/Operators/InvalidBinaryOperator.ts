/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/// <reference path="../../../../../../../reference.d.ts" />
namespace Io.Oidis.XCppLint.Rules.Errors.Whitespace.Operators {
    "use strict";
    import LogIt = Io.Oidis.Commons.Utils.LogIt;
    import BaseLintRule = Io.Oidis.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Io.Oidis.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Io.Oidis.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Io.Oidis.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Io.Oidis.Commons.Utils.StringUtils;

    export class InvalidBinaryOperator extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.WHITESPACE.OPERATORS.INVALID_BINARY_OPERATOR);
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            let match : RegExpExecArray = /(.)\+(.)/gm.exec($processingInfo.Child().body);
            if (match) {
                if (!StringUtils.Contains($processingInfo.Child().body, "operator+") &&
                    !StringUtils.Contains($processingInfo.Child().body, "++") &&
                    !StringUtils.Contains($processingInfo.Child().body, "+=") &&
                    (match[1] !== " " || match[2] !== " ")) {
                    $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                }
            }

            match = /(.)-([^0-9])/gm.exec($processingInfo.Child().body);
            if (match) {
                if (!StringUtils.Contains($processingInfo.Child().body, "operator-") &&
                    !StringUtils.Contains($processingInfo.Child().body, "--") &&
                    !StringUtils.Contains($processingInfo.Child().body, "-=") &&
                    !StringUtils.Contains($processingInfo.Child().body, "->") &&
                    (match[1] !== " " || match[2] !== " ")) {
                    $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                }
            }

            match = /([^'])\/([^'])/gm.exec($processingInfo.Child().body);
            if (match) {
                if (!StringUtils.Contains($processingInfo.Child().body, "operator/") &&
                    !StringUtils.Contains($processingInfo.Child().body, "/=") &&
                    ((match[1] !== " " || match[2] !== " ") && match[2] !== "\n")) {
                    $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                }
            }

            match = /(.)%(.)/gm.exec($processingInfo.Child().body);
            if (match) {
                if (!StringUtils.Contains($processingInfo.Child().body, "operator%") &&
                    !StringUtils.Contains($processingInfo.Child().body, "%=") &&
                    (match[1] !== " " || match[2] !== " ")) {
                    $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                }
            }
        }
    }
}
