/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/// <reference path="../../../../../../reference.d.ts" />
namespace Io.Oidis.XCppLint.Rules.Errors.Whitespace {
    "use strict";
    import LogIt = Io.Oidis.Commons.Utils.LogIt;
    import BaseLintRule = Io.Oidis.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Io.Oidis.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Io.Oidis.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Io.Oidis.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Io.Oidis.Commons.Utils.StringUtils;

    export class Pointers extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.WHITESPACE.POINTERS);
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            if (StringUtils.Contains($processingInfo.Child().body, " ->") ||
                StringUtils.Contains($processingInfo.Child().body, "-> ")) {
                $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
            }

            if (StringUtils.Contains($processingInfo.Child().body, " .") ||
                (StringUtils.Contains($processingInfo.Child().body, ". ") &&
                    !StringUtils.Contains($processingInfo.Child().body, "... "))) {
                if (StringUtils.StartsWith($processingInfo.Child().body, ".") &&
                    StringUtils.EndsWith($processingInfo.Parent().body, "{")) {
                    return;
                }
                $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
            }
        }
    }
}
