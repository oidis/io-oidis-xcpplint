/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/// <reference path="../../../../../../../reference.d.ts" />
namespace Io.Oidis.XCppLint.Rules.Errors.Whitespace.Newline {
    "use strict";
    import BaseLintRule = Io.Oidis.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Io.Oidis.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Io.Oidis.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Io.Oidis.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Io.Oidis.Commons.Utils.StringUtils;
    import IToken = Io.Oidis.XCppLint.Interfaces.IToken;

    export class DoWhileAfterBrace extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.WHITESPACE.NEWLINE.DO_WHILE_AFTER_BRACE);
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            const prevChild : IToken = $processingInfo.PrevChild();

            if (prevChild && StringUtils.Contains(prevChild.body, "do {")) {
                const lastPrevChild : IToken = this.getLastChild(prevChild);
                if (lastPrevChild) {
                    if (lastPrevChild.line !== $processingInfo.Child().line) {
                        $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                    }
                }
            }
        }

        private getLastChild($parent : IToken) : any {
            if (!$parent) {
                return null;
            }

            if ($parent.children.length === 0) {
                return null;
            }

            return $parent.children[$parent.children.length - 1];
        }
    }
}
