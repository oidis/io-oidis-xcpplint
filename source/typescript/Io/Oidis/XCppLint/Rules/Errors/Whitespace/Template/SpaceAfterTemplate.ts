/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/// <reference path="../../../../../../../reference.d.ts" />
namespace Io.Oidis.XCppLint.Rules.Errors.Whitespace.Template {
    "use strict";
    import BaseLintRule = Io.Oidis.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Io.Oidis.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Io.Oidis.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Io.Oidis.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Io.Oidis.Commons.Utils.StringUtils;
    import TemplateBefore = Io.Oidis.XCppLint.Rules.Before.TemplateBefore;
    import TemplateProcessingInfo = Io.Oidis.XCppLint.Structures.ProcessingInfo.TemplateProcessingInfo;

    export class SpaceAfterTemplate extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.WHITESPACE.TEMPLATE.SPACE_AFTER_TEMPLATE);
            this.setOnBefore(TemplateBefore.getInstance());
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            const pInfo : TemplateProcessingInfo = <TemplateProcessingInfo>this.getOnBefore().getResults($processingInfo);

            if (pInfo.Matches() && StringUtils.EndsWith(pInfo.Matches()[1], " ")) {
                $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
            }
        }
    }
}
