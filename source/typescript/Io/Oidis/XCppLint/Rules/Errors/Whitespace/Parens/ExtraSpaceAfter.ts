/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/// <reference path="../../../../../../../reference.d.ts" />
namespace Io.Oidis.XCppLint.Rules.Errors.Whitespace.Parens {
    "use strict";
    import BaseLintRule = Io.Oidis.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Io.Oidis.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Io.Oidis.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Io.Oidis.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import ParensBefore = Io.Oidis.XCppLint.Rules.Before.ParensBefore;
    import ParensProcessingInfo = Io.Oidis.XCppLint.Structures.ProcessingInfo.ParensProcessingInfo;

    export class ExtraSpaceAfter extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.WHITESPACE.PARENS.EXTRA_SPACE_AFTER);
            this.setOnBefore(ParensBefore.getInstance());
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            const pInfo : ParensProcessingInfo = <ParensProcessingInfo>this.getOnBefore().getResults($processingInfo);

            if (pInfo.CommonCondition()) {
                if (/\w\s*\([ ](?!\s*\\$)/.test(pInfo.Fncall())) {
                    $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                } else if (/\( +(?!( *\\)|\()/.test(pInfo.Fncall())) {
                    $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                }
            }
        }
    }
}
