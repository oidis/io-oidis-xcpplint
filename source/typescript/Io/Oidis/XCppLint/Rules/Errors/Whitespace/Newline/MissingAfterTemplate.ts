/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/// <reference path="../../../../../../../reference.d.ts" />
namespace Io.Oidis.XCppLint.Rules.Errors.Whitespace.Newline {
    "use strict";
    import BaseLintRule = Io.Oidis.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Io.Oidis.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Io.Oidis.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Io.Oidis.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Io.Oidis.Commons.Utils.StringUtils;

    export class MissingAfterTemplate extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.WHITESPACE.NEWLINE.MISSING_AFTER_TEMPLATE);
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            const matches : RegExpExecArray = this.parseTemplate($processingInfo.Child().body);
            if (!matches) {
                return;
            }

            if (!StringUtils.StartsWith(matches[2], "\n")) {
                $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
            }
        }

        public Fix($pinfo : BaseProcessingInfo) : boolean {
            const matches : RegExpExecArray = this.parseTemplate($pinfo.Child().body);
            $pinfo.Child().body = matches[1] + "\n" + this.generateSpaces($pinfo.Child().nesting * 4) + matches[2].trim();
            return true;
        }

        private parseTemplate($body : string) : RegExpExecArray {
            return /(template\s*<\s*[\w\s,]*\s*>)(\s*.*)/.exec($body);
        }
    }
}
