/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/// <reference path="../../../../../../../reference.d.ts" />
namespace Io.Oidis.XCppLint.Rules.Errors.Readability.Wui {
    "use strict";
    import TokenType = Io.Oidis.XCppLint.Enums.TokenType;
    import LogIt = Io.Oidis.Commons.Utils.LogIt;
    import BaseLintRule = Io.Oidis.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Io.Oidis.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Io.Oidis.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Io.Oidis.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Io.Oidis.Commons.Utils.StringUtils;

    export class VisibilitySpecComment extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.READABILITY.WUI.VISIBILITY_SPEC_COMMENT);
            this.setSensitivity(TokenType.COMMENT);
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            const nextToken = $processingInfo.NextToken();
            if (nextToken &&
                (StringUtils.StartsWith(nextToken.body.trim(), "public:") ||
                    StringUtils.StartsWith(nextToken.body.trim(), "protected:") ||
                    StringUtils.StartsWith(nextToken.body.trim(), "private:"))) {
                $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
            }
        }
    }
}
