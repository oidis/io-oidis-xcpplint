/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/// <reference path="../../../../../../../reference.d.ts" />
namespace Io.Oidis.XCppLint.Rules.Errors.Readability.Casting {
    "use strict";
    import LogIt = Io.Oidis.Commons.Utils.LogIt;
    import BaseLintRule = Io.Oidis.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Io.Oidis.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Io.Oidis.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Io.Oidis.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;

    export class DereferencedCast extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.READABILITY.CASTING.DEREFERENCED_CAST);
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            let match : RegExpExecArray =
                /(?:[^\w]&\(([^)*][^)]*)\)[\w(])|(?:[^\w]&(static|dynamic|down|reinterpret)_cast\b)/.exec($processingInfo.Child().body);

            if (match !== null) {
                match = /^(.*&(?:static|dynamic|down|reinterpret)_cast\b)</gm.exec($processingInfo.Child().body);

                if (match !== null) {
                    $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                }
            }
        }
    }
}
