/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/// <reference path="../../../../../../../reference.d.ts" />
namespace Io.Oidis.XCppLint.Rules.Errors.Readability.Wui {
    "use strict";
    import LogIt = Io.Oidis.Commons.Utils.LogIt;
    import BaseLintRule = Io.Oidis.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Io.Oidis.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Io.Oidis.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Io.Oidis.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Io.Oidis.Commons.Utils.StringUtils;
    import ArrayList = Io.Oidis.Commons.Primitives.ArrayList;
    import SpecType = Io.Oidis.XCppLint.Enums.SpecType;
    import WuiProcessingInfo = Io.Oidis.XCppLint.Structures.ProcessingInfo.WuiProcessingInfo;
    import WuiBefore = Io.Oidis.XCppLint.Rules.Before.WuiBefore;
    import ClassMember = Io.Oidis.XCppLint.Structures.ClassMember;

    export class MethodNaming extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.READABILITY.WUI.METHOD_NAMING);
            this.setOnBefore(WuiBefore.getInstance());
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            const pInfo : WuiProcessingInfo = <WuiProcessingInfo>this.getOnBefore().getResults($processingInfo);
            const methods : ArrayList<ClassMember> = pInfo.Members();

            if (!methods || methods.IsEmpty()) {
                return;
            }

            for (const method of methods.getAll()) {
                const methodSpec : SpecType = method.Member().specType;
                const matches : RegExpExecArray =
                    /\s*(\w(?:\w|::|<|>|\s)+)\s((\w|::|\*|\&|operator=|<<|>>)*)(\(.*)/gm.exec(method.Member().body);

                if (matches) {
                    const methodName : string = StringUtils.Split(matches[2], "::").reverse()[0];
                    if (methodSpec === SpecType.PUBLIC) {
                        if (!/^(\*)?[A-Z]/.test(methodName)) {
                            if (!/^[*&]*[gs]et/gm.test(methodName)) {
                                $onError(this.getErrorObject(), method.Member().line, -1);
                            }
                        } else if (/^[*&]*[GS]et/.test(methodName)) {
                            $onError(this.getErrorObject(), method.Member().line, -1);
                        }
                    } else if (methodSpec === SpecType.PROTECTED || methodSpec === SpecType.PRIVATE) {
                        if (!/^(\*)?[a-z]/.test(methodName)) {
                            $onError(this.getErrorObject(), method.Member().line, -1);
                        }
                    }
                }
            }
        }
    }
}
