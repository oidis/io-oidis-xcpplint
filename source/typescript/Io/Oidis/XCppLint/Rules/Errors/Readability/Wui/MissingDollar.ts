/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/// <reference path="../../../../../../../reference.d.ts" />
namespace Io.Oidis.XCppLint.Rules.Errors.Readability.Wui {
    "use strict";
    import LogIt = Io.Oidis.Commons.Utils.LogIt;
    import BaseLintRule = Io.Oidis.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Io.Oidis.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Io.Oidis.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Io.Oidis.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Io.Oidis.Commons.Utils.StringUtils;
    import WuiBefore = Io.Oidis.XCppLint.Rules.Before.WuiBefore;
    import WuiProcessingInfo = Io.Oidis.XCppLint.Structures.ProcessingInfo.WuiProcessingInfo;

    export class MissingDollar extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.READABILITY.WUI.MISSING_DOLLAR);
            this.setOnBefore(WuiBefore.getInstance());
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            const pInfo : WuiProcessingInfo = <WuiProcessingInfo>this.getOnBefore().getResults($processingInfo);

            const subArgs : string[] = pInfo.SubArg();

            subArgs.forEach(($value : string) : void => {
                if (!StringUtils.StartsWith($value, "$")) {
                    $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                }
            });
        }
    }
}
