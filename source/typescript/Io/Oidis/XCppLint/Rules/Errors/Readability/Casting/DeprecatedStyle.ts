/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/// <reference path="../../../../../../../reference.d.ts" />
namespace Io.Oidis.XCppLint.Rules.Errors.Readability.Casting {
    "use strict";
    import LogIt = Io.Oidis.Commons.Utils.LogIt;
    import BaseLintRule = Io.Oidis.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Io.Oidis.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Io.Oidis.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Io.Oidis.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Io.Oidis.Commons.Utils.StringUtils;

    export class DeprecatedStyle extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.READABILITY.CASTING.DEPRECATED_STYLE);
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            const regex : RegExp =
                /(\bnew\s+(?:const\s+)?|\S<\s*(?:const\s+)?)?\b(int|float|double|bool|char|int32|uint32|int64|uint64)(\([^)].*)/gm;
            const match : RegExpExecArray = regex.exec($processingInfo.Child().body.trim());

            const expectingFunctionArgs = ($body : string) : boolean => {
                return (/\bstd::m?function\s*\<\s*$/gm.test($body));
            };

            const expectingFunction : boolean = expectingFunctionArgs($processingInfo.Child().body);

            if (match && !expectingFunction) {
                if (/\([^()]+\)\s*\[/gm.test(match[3])) {
                    return;
                }

                const matchNewOrTemplate = match[1];
                const matchedType = match[2];
                const matchedFuncPtr = match[3];

                if (!matchNewOrTemplate &&
                    !(matchedFuncPtr &&
                        (/\((?:[^() ]+::\s*\*\s*)?[^() ]+\)\s*\(/gm.test(matchedFuncPtr) ||
                            StringUtils.StartsWith(matchedFuncPtr, "(*)"))) &&
                    !(new RegExp("\s*using\s+\S+\s*=\s*" + matchedType, "gm")).test($processingInfo.Child().body) &&
                    !(new RegExp("new\\(\\S+\\)\\s*" + matchedType, "gm")).test($processingInfo.Child().body)) {
                    $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                }
            }
        }
    }
}
