/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/// <reference path="../../../../../../../reference.d.ts" />
namespace Io.Oidis.XCppLint.Rules.Errors.Readability.Braces {
    "use strict";
    import LogIt = Io.Oidis.Commons.Utils.LogIt;
    import BaseLintRule = Io.Oidis.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Io.Oidis.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Io.Oidis.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Io.Oidis.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Io.Oidis.Commons.Utils.StringUtils;
    import TokenType = Io.Oidis.XCppLint.Enums.TokenType;

    export class NewLineIf extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.READABILITY.BRACES.NEW_LINE_IF);
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            if (StringUtils.StartsWith($processingInfo.Child().body.trim(), "if (") &&
                !StringUtils.StartsWith($processingInfo.Child().body, "\n")) {
                if ($processingInfo.PrevToken() && ($processingInfo.PrevToken().type === TokenType.DIRECTIVE ||
                    $processingInfo.PrevToken().type === TokenType.COMMENT)
                    && StringUtils.EndsWith($processingInfo.PrevToken().body, "\n")) {
                    return;
                }
                $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
            }
        }
    }
}
