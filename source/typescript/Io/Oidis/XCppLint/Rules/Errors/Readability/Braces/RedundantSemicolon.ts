/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/// <reference path="../../../../../../../reference.d.ts" />
namespace Io.Oidis.XCppLint.Rules.Errors.Readability.Braces {
    "use strict";
    import IToken = Io.Oidis.XCppLint.Interfaces.IToken;
    import LogIt = Io.Oidis.Commons.Utils.LogIt;
    import BaseLintRule = Io.Oidis.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Io.Oidis.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Io.Oidis.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Io.Oidis.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Io.Oidis.Commons.Utils.StringUtils;

    export class RedundantSemicolon extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.READABILITY.BRACES.REDUNDANT_SEMICOLON);
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            if ($processingInfo.Child().body === ";" ||
                $processingInfo.Child().body.trim() === "};") {
                const parent : IToken = $processingInfo.Parent();

                if (!parent) {
                    return;
                }

                if (BaseLintRule.isIf(parent.body) ||
                    BaseLintRule.isFor(parent.body) ||
                    BaseLintRule.isWhile(parent.body) ||
                    StringUtils.StartsWith(parent.body.trim(), "switch") ||
                    StringUtils.StartsWith(parent.body.trim(), "else") ||
                    StringUtils.StartsWith(parent.body.trim(), "namespace") ||
                    parent.body.trim() === "{" ||
                    BaseLintRule.isFunction(parent.body)) {
                    $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
                }
            }
        }
    }
}
