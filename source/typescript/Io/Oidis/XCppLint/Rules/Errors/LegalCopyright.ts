/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/// <reference path="../../../../../reference.d.ts" />
namespace Io.Oidis.XCppLint.Rules.Errors {
    "use strict";
    import IErrorReporterObject = Io.Oidis.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Io.Oidis.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Io.Oidis.Commons.Utils.StringUtils;
    import BaseLintRule = Io.Oidis.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Io.Oidis.XCppLint.Core.ERRORS;

    export class LegalCopyright extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.LEGAL_COPYRIGHT);
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            if ($processingInfo.Child().childIndex === 0 && $processingInfo.Parent().body === "") {
                if (!StringUtils.Contains($processingInfo.Child().body, "Copyright")) {
                    $onError(this.getErrorObject(), 0, -1);
                }
            }
        }
    }
}
