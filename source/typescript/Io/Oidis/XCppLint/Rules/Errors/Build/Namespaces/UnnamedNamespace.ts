/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2020 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/// <reference path="../../../../../../../reference.d.ts" />
namespace Io.Oidis.XCppLint.Rules.Errors.Build.Namespaces {
    "use strict";
    import LogIt = Io.Oidis.Commons.Utils.LogIt;
    import BaseLintRule = Io.Oidis.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Io.Oidis.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Io.Oidis.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Io.Oidis.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Io.Oidis.Commons.Utils.StringUtils;

    export class UnnamedNamespace extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.BUILD.NAMESPACES.UNNAMED_NAMESPACE);
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            if (this.isHeaderFile($processingInfo.FileName()) &&
                /namespace\s*{/gm.test($processingInfo.Child().body)) {
                $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
            }
        }

        private isHeaderFile($fileName : string) : boolean {
            return StringUtils.EndsWith($fileName, ".hpp") ||
                StringUtils.EndsWith($fileName, ".h");
        }
    }
}
