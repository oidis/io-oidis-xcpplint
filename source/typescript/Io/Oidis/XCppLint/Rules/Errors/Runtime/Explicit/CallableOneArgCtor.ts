/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/// <reference path="../../../../../../../reference.d.ts" />
namespace Io.Oidis.XCppLint.Rules.Errors.Runtime.Explicit {
    "use strict";
    import LogIt = Io.Oidis.Commons.Utils.LogIt;
    import BaseLintRule = Io.Oidis.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Io.Oidis.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Io.Oidis.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Io.Oidis.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Io.Oidis.Commons.Utils.StringUtils;
    import ExplicitProcessingInfo = Io.Oidis.XCppLint.Structures.ProcessingInfo.ExplicitProcessingInfo;
    import ExplicitBefore = Io.Oidis.XCppLint.Rules.Before.ExplicitBefore;

    export class CallableOneArgCtor extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.RUNTIME.EXPLICIT.CALLABLE_ONE_ARG_CTOR);
            this.setOnBefore(ExplicitBefore.getInstance());
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            if (!StringUtils.StartsWith($processingInfo.Parent().body.trim(), "class")) {
                return;
            }

            const pInfo : ExplicitProcessingInfo = (<ExplicitProcessingInfo>this.getOnBefore().getResults($processingInfo));

            if (!pInfo.IsMarkedExplicit() &&
                pInfo.OneArgCtor() &&
                !pInfo.InitializerListCtor() &&
                !pInfo.CopyCtor() &&
                pInfo.DefaultedArgs().length !== 0) {
                $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
            }
        }
    }
}
