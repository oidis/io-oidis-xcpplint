/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/// <reference path="../../../../../../../reference.d.ts" />
namespace Io.Oidis.XCppLint.Rules.Errors.Runtime.String {
    "use strict";
    import LogIt = Io.Oidis.Commons.Utils.LogIt;
    import BaseLintRule = Io.Oidis.XCppLint.Core.Model.BaseLintRule;
    import ERRORS = Io.Oidis.XCppLint.Core.ERRORS;
    import IErrorReporterObject = Io.Oidis.XCppLint.Interfaces.IErrorReporterObject;
    import BaseProcessingInfo = Io.Oidis.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import StringUtils = Io.Oidis.Commons.Utils.StringUtils;
    import StringBefore = Io.Oidis.XCppLint.Rules.Before.StringBefore;
    import StringProcessingInfo = Io.Oidis.XCppLint.Structures.ProcessingInfo.StringProcessingInfo;

    export class NotPermitted extends BaseLintRule {

        constructor() {
            super();
            this.setErrorObject(ERRORS.RUNTIME.STRING.NOT_PERMITTED);
            this.setOnBefore(StringBefore.getInstance());
        }

        public Process($processingInfo : BaseProcessingInfo,
                       $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void {
            const pInfo : StringProcessingInfo = <StringProcessingInfo>this.getOnBefore().getResults($processingInfo);

            if (pInfo.CommonCondition() &&
                !StringUtils.Contains($processingInfo.Child().body, "const")) {
                $onError(this.getErrorObject(), $processingInfo.Child().line, -1);
            }
        }
    }
}
