/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/// <reference path="../../../../../reference.d.ts" />
namespace Io.Oidis.XCppLint.Structures.ProcessingInfo {
    "use strict";
    import ObjectValidator = Io.Oidis.Commons.Utils.ObjectValidator;

    export class TemplateProcessingInfo extends BaseProcessingInfo {
        private matches : RegExpExecArray;

        public Matches($matches? : RegExpExecArray) : RegExpExecArray {
            if (!ObjectValidator.IsEmptyOrNull($matches)) {
                this.matches = $matches;
            }
            return this.matches;
        }

        public Clear() : void {
            this.matches = null;
        }
    }
}
