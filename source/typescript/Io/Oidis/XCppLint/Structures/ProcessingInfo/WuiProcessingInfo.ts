/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/// <reference path="../../../../../reference.d.ts" />
namespace Io.Oidis.XCppLint.Structures.ProcessingInfo {
    "use strict";
    import ArrayList = Io.Oidis.Commons.Primitives.ArrayList;
    import ClassMember = Io.Oidis.XCppLint.Structures.ClassMember;
    import ObjectValidator = Io.Oidis.Commons.Utils.ObjectValidator;

    export class WuiProcessingInfo extends BaseProcessingInfo {
        private subArg : string[];
        private joinedLine : string;
        private members : ArrayList<ClassMember>;
        private publicMembers : ArrayList<ClassMember>;
        private protectedMembers : ArrayList<ClassMember>;
        private privateMembers : ArrayList<ClassMember>;
        private publicTargetMembers : ArrayList<ClassMember>;
        private protectedTargetMembers : ArrayList<ClassMember>;
        private privateTargetMembers : ArrayList<ClassMember>;

        constructor() {
            super();
            this.subArg = [];
            this.members = new ArrayList<ClassMember>();
            this.publicMembers = new ArrayList<ClassMember>();
            this.protectedMembers = new ArrayList<ClassMember>();
            this.privateMembers = new ArrayList<ClassMember>();
            this.publicTargetMembers = new ArrayList<ClassMember>();
            this.protectedTargetMembers = new ArrayList<ClassMember>();
            this.privateTargetMembers = new ArrayList<ClassMember>();
        }

        public Clear() : void {
            this.subArg = [];
            this.joinedLine = "";
            this.members.Clear();
            this.publicMembers.Clear();
            this.protectedMembers.Clear();
            this.privateMembers.Clear();
            this.publicTargetMembers.Clear();
            this.protectedTargetMembers.Clear();
            this.privateTargetMembers.Clear();
        }

        public SubArg($subArg? : string[]) : string[] {
            if (!ObjectValidator.IsEmptyOrNull($subArg)) {
                this.subArg = $subArg;
            }
            return this.subArg;
        }

        public JoinedLine($joinedLine? : string) : string {
            if (!ObjectValidator.IsEmptyOrNull($joinedLine)) {
                this.joinedLine = $joinedLine;
            }
            return this.joinedLine;
        }

        public Members($members? : ArrayList<ClassMember>) : ArrayList<ClassMember> {
            if (!ObjectValidator.IsEmptyOrNull($members)) {
                this.members = $members;
            }
            return this.members;
        }

        public PublicMembers() : ArrayList<ClassMember> {
            return this.publicMembers;
        }

        public PublicTargetMembers() : ArrayList<ClassMember> {
            return this.publicTargetMembers;
        }

        public ProtectedMembers() : ArrayList<ClassMember> {
            return this.protectedMembers;
        }

        public ProtectedTargetMembers() : ArrayList<ClassMember> {
            return this.protectedTargetMembers;
        }

        public PrivateMembers() : ArrayList<ClassMember> {
            return this.privateMembers;
        }

        public PrivateTargetMembers() : ArrayList<ClassMember> {
            return this.privateTargetMembers;
        }
    }
}
