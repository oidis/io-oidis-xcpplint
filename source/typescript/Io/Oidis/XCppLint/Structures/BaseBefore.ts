/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/// <reference path="../../../../reference.d.ts" />
namespace Io.Oidis.XCppLint.Structures {
    "use strict";
    import LogIt = Io.Oidis.Commons.Utils.LogIt;
    import BaseProcessingInfo = Io.Oidis.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;
    import ObjectValidator = Io.Oidis.Commons.Utils.ObjectValidator;
    import BaseObject = Io.Oidis.Commons.Primitives.BaseObject;

    export abstract class BaseBefore extends BaseObject {
        private done : boolean;

        constructor() {
            super();
            this.done = false;
        }

        /**
         * @param {BaseProcessingInfo} $processingInfo Additional information neccessary to perform "before" computation.
         * Also runs the computation.
         * @return {BaseProcessingInfo} Return extended BaseProcessingInfo, containing rule-specific additional information.
         */
        public getResults($processingInfo : BaseProcessingInfo) : BaseProcessingInfo {
            return null;
        }

        public Reset() : void {
            this.Done(false);
        }

        protected Done($done? : boolean) : boolean {
            if (!ObjectValidator.IsEmptyOrNull($done)) {
                this.done = $done;
            }
            return this.done;
        }
    }
}
