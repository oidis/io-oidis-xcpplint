/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/// <reference path="../../../../reference.d.ts" />
namespace Io.Oidis.XCppLint.Structures {
    "use strict";

    import IToken = Io.Oidis.XCppLint.Interfaces.IToken;
    import ObjectValidator = Io.Oidis.Commons.Utils.ObjectValidator;

    export class ClassMember {
        private member : IToken;
        private comment : IToken;

        /**
         * @param {IToken} $member If set, set this.member to this value.
         * @return {IToken} Returns a member of a class.
         */
        public Member($member? : IToken) : IToken {
            if (!ObjectValidator.IsEmptyOrNull($member)) {
                this.member = $member;
            }
            return this.member;
        }

        /**
         * @param {IToken} $comment Set associated comment.
         * @return {IToken} Return a token associated with a memeber of a class.
         */
        public Comment($comment? : IToken) : IToken {
            if (!ObjectValidator.IsEmptyOrNull($comment)) {
                this.comment = $comment;
            }
            return this.comment;
        }
    }
}
