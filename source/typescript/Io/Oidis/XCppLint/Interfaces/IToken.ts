/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/// <reference path="../../../../reference.d.ts" />
namespace Io.Oidis.XCppLint.Interfaces {
    "use strict";
    import TokenType = Io.Oidis.XCppLint.Enums.TokenType;
    import SpecType = Io.Oidis.XCppLint.Enums.SpecType;

    export interface IToken {
        body : string;
        childIndex : number;
        children : IToken[];
        type : TokenType;
        line : number;
        nesting : number;
        originalBody : string;
        specType : SpecType;
        parent : IToken;
    }
}
