/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/// <reference path="../../../../reference.d.ts" />
namespace Io.Oidis.XCppLint.Interfaces {
    "use strict";
    import Severity = Io.Oidis.XCppLint.Enums.Severity;
    import Priority = Io.Oidis.XCppLint.Enums.Priority;
    import Config = Io.Oidis.XCppLint.Core.Config;
    import BaseProcessingInfo = Io.Oidis.XCppLint.Structures.ProcessingInfo.BaseProcessingInfo;

    export interface IBaseLintRule extends Io.Oidis.Commons.Interfaces.IBaseObject {
        Process($processingInfo : BaseProcessingInfo,
                $onError : ($message : IErrorReporterObject, $line : number, $column : number) => void) : void;

        Severity($severity? : Severity) : void;

        Priority($priority? : Priority) : void;

        Message() : string;

        setErrorObject($object : IErrorReporterObject) : void;

        getErrorObject() : IErrorReporterObject;

        ResetBefore() : void;

        IsSensitiveTo($token : IToken) : boolean;

        Fix($pinfo : BaseProcessingInfo) : boolean;

        LoadConfig($config : Config) : void;
    }
}
