/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/// <reference path="../../../../reference.d.ts" />
namespace Io.Oidis.XCppLint.Interfaces {
    "use strict";
    import Severity = Io.Oidis.XCppLint.Enums.Severity;
    import Priority = Io.Oidis.XCppLint.Enums.Priority;

    export interface IErrorReporterObject {
        message : string;
        severity : Severity;
        priority : Priority;
    }
}
