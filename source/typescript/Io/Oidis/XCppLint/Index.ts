/* ********************************************************************************************************* *
 *
 * Copyright 2017-2018 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/// <reference path="../../../reference.d.ts" />
namespace Io.Oidis.XCppLint {
    "use strict";
    import Echo = Io.Oidis.Commons.Utils.Echo;

    export class Index extends Io.Oidis.Commons.HttpProcessor.Resolvers.BaseHttpResolver {

        protected resolver() : void {
            /* dev:start */
            Echo.Print("<H3>Runtime tests</H3>");
            Echo.Println("<a href=\"#" + this.createLink("/web/LinterTest") + "\">Linter test</a>");
            /* dev:end */
        }
    }
}
