/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/// <reference path="../../../../reference.d.ts" />
namespace Io.Oidis.XCppLint.Core {
    "use strict";
    import BaseObject = Io.Oidis.Commons.Primitives.BaseObject;
    import IToken = Io.Oidis.XCppLint.Interfaces.IToken;
    import LintError = Io.Oidis.XCppLint.Core.Model.LintError;
    import ArrayList = Io.Oidis.Commons.Primitives.ArrayList;
    import ReplacementPair = Io.Oidis.XCppLint.Structures.ReplacementPair;

    export class Linter extends BaseObject {
        private errors : LintError[];

        constructor() {
            super();
            this.errors = [];
        }

        /**
         * @param {string} $data Input data containing c++ code that is supposed to be linted.
         * @param {string }$filePath Path to the file that is linted.
         * @param {any} $configData JSON with config.
         * @param {function} $onFixedFile Callback to handle fixed file.
         */
        public Run($data : string, $filePath : string, $configData : any,
                   $onFixedFile? : ($fixedFileData : string) => void) : void {
            const config : Config = new Config(new ErrorReporter());
            config.Load($configData);

            const filename = $filePath.replace(/^.*[\\\/]/, "");

            if (config.HasValidExtension(filename) && !config.IsIgnoredFile(filename)) {
                const codeBlockParser : CodeBlocksParser = new CodeBlocksParser();
                codeBlockParser.LoadConfig(config);

                const tokenMapLinter : TokenMapLinter = new TokenMapLinter(config, filename);

                codeBlockParser.setOwner($filePath);
                tokenMapLinter.setOwner($filePath);

                const replacementPairs : ArrayList<ReplacementPair> = new ArrayList<ReplacementPair>();
                const tokenMap : IToken = codeBlockParser.Parse($data, replacementPairs);

                tokenMapLinter.Process(tokenMap);

                if (config.HasErrorsToFix()) {
                    tokenMapLinter.fixErrors();

                    const formattedFile : string = tokenMapLinter.getFinalFile(tokenMap, replacementPairs);

                    if ($onFixedFile) {
                        $onFixedFile(formattedFile);
                    }
                }

                this.errors = tokenMapLinter.getAllErrors().getAll().concat(codeBlockParser.getAllErrors().getAll());
            }
        }

        /**
         * @return {LintError[]} Return array of linting errors.
         */
        public getErrors() : LintError[] {
            return this.errors;
        }

        public CleanErrors() : void {
            this.errors = [];
        }
    }
}
