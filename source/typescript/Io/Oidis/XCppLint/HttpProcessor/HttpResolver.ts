/* ********************************************************************************************************* *
 *
 * Copyright 2017 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/// <reference path="../../../../reference.d.ts" />
namespace Io.Oidis.XCppLint.HttpProcessor {
    "use strict";

    export class HttpResolver extends Io.Oidis.Commons.HttpProcessor.HttpResolver {

        protected getStartupResolvers() : any {
            const resolvers : any = super.getStartupResolvers();
            resolvers["/"] = Io.Oidis.XCppLint.Index;
            resolvers["/index"] = Io.Oidis.XCppLint.Index;
            resolvers["/web/"] = Io.Oidis.XCppLint.Index;
            resolvers["/web/LinterTest"] = RuntimeTests.LinterTest;
            return resolvers;
        }
    }
}
