/** WARNING: this file has been automatically generated from typescript interfaces, which exist in this package. */

/// <reference path="reference.d.ts" />
/* tslint:disable: variable-name no-use-before-declare only-arrow-functions */
namespace Io.Oidis.XCppLint.Interfaces {
    "use strict";
    export let IErrorReporterObject : Io.Oidis.Commons.Interfaces.Interface =
        function () : Io.Oidis.Commons.Interfaces.Interface {
            return Io.Oidis.Commons.Interfaces.Interface.getInstance([
                "message",
                "severity",
                "priority"
            ]);
        }();
}

namespace Io.Oidis.XCppLint.Interfaces {
    "use strict";
    export let IToken : Io.Oidis.Commons.Interfaces.Interface =
        function () : Io.Oidis.Commons.Interfaces.Interface {
            return Io.Oidis.Commons.Interfaces.Interface.getInstance([
                "body",
                "childIndex",
                "children",
                "type",
                "line",
                "nesting",
                "originalBody",
                "specType",
                "parent"
            ]);
        }();
}

namespace Io.Oidis.XCppLint.Interfaces {
    "use strict";
    export let IBaseLintRule : Io.Oidis.Commons.Interfaces.Interface =
        function () : Io.Oidis.Commons.Interfaces.Interface {
            return Io.Oidis.Commons.Interfaces.Interface.getInstance([
                "Process",
                "Severity",
                "Priority",
                "Message",
                "setErrorObject",
                "getErrorObject",
                "ResetBefore",
                "IsSensitiveTo",
                "Fix",
                "LoadConfig"
            ], Io.Oidis.Commons.Interfaces.IBaseObject);
        }();
}

/* tslint:enable */
