/* ********************************************************************************************************* *
 *
 * Copyright 2014-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2019 NXP
 * Copyright 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/// <reference path="../../../reference.d.ts" />
namespace Io.Oidis.XCppLint {
    "use strict";
    import LintError = Io.Oidis.XCppLint.Core.Model.LintError;
    import ObjectValidator = Io.Oidis.Commons.Utils.ObjectValidator;
    import IErrorReporterObject = Io.Oidis.XCppLint.Interfaces.IErrorReporterObject;
    import ArrayList = Io.Oidis.Commons.Primitives.ArrayList;
    import IToken = Io.Oidis.XCppLint.Interfaces.IToken;
    import UnitTestEnvironmentArgs = Io.Oidis.Commons.UnitTestEnvironmentArgs;

    export class UnitTestLoader extends Loader {
        protected initEnvironment() : UnitTestEnvironmentArgs {
            return new UnitTestEnvironmentArgs();
        }
    }

    export class UnitTestRunner extends Io.Oidis.UnitTestRunner {

        public readJsonFromFile($path : string, $source : any) : any {
            return JSON.parse(builder.getFileSystemHandler().Read($source.getAbsoluteRoot() + "/" + $path).toString());
        }

        public readFileRelative($path : string, $source : any) : string {
            return builder.getFileSystemHandler().Read($source.getAbsoluteRoot() + "/" + $path).toString();
        }

        public readFileAbsolute($path : string) : string {
            return builder.getFileSystemHandler().Read($path).toString();
        }

        public Expand($pattern : string | string[]) : string[] {
            return builder.getFileSystemHandler().Expand($pattern);
        }

        public isErrorInArray($errorToFind : LintError | any, $arrayToSearch : ArrayList<LintError>) : boolean {
            let lineToFind : number;
            let messageToFind : string;

            try {
                ObjectValidator.IsObject($errorToFind);
                (<LintError>$errorToFind).IsTypeOf(LintError);

                lineToFind = (<LintError>$errorToFind).getLine();
                messageToFind = (<LintError>$errorToFind).getMessage();
            } catch (exception) {
                lineToFind = (<ILintError>$errorToFind).line;
                if (ObjectValidator.IsString((<ILintError>$errorToFind).message)) {
                    messageToFind = (<string>(<ILintError>$errorToFind).message);
                } else {
                    messageToFind = (<IErrorReporterObject>(<ILintError>$errorToFind).message).message;
                }
            }

            for (const elem of $arrayToSearch.getAll()) {
                if (elem.getMessage() === messageToFind &&
                    elem.getLine() === lineToFind) {
                    return true;
                }
            }

            return false;
        }

        public compareJsonsByChildBody($json1 : any, $json2Path : string) : void {
            this.serializeJsonsByChildBody($json1, this.readJsonFromFile(
                $json2Path, this),
                ($outArray1 : string[], $outArray2 : string[]) : void => {
                    $outArray1.forEach(($value : string, $index : number) : void => {
                        assert.equal($value, $outArray2[$index]);
                    });
                });
        }

        protected initLoader() : void {
            super.initLoader(UnitTestLoader);
        }

        private serializeJsonsByChildBody($json1 : any, $json2 : any,
                                          $callback : ($arr1 : string[], $arr2 : string[]) => void) : void {
            const outArray1 : string[] = [];
            const outArray2 : string[] = [];

            const serialize = ($parent : IToken, $out : string[]) : void => {
                for (const child of $parent.children) {
                    $out.push(child.body);
                    serialize(child, $out);
                }
            };

            serialize($json1, outArray1);
            serialize($json2, outArray2);

            $callback(outArray1, outArray2);
        }
    }

    interface ILintError {
        message : string | IErrorReporterObject;
        line : number;
    }
}
