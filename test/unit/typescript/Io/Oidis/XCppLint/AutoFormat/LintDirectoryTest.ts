/* ********************************************************************************************************* *
 *
 * Copyright 2018 NXP
 * Copyright 2019-2021 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/// <reference path="../../../../reference.d.ts" />
namespace Io.Oidis.XCppLint.AutoFormat {
    "use strict";
    import LogIt = Io.Oidis.Commons.Utils.LogIt;
    import Echo = Io.Oidis.Commons.Utils.Echo;
    import LintError = Io.Oidis.XCppLint.Core.Model.LintError;
    import Linter = Io.Oidis.XCppLint.Core.Linter;

    export class LintDirectoryTest extends UnitTestRunner {
        private config : any = {
            extensions  : [
                "cpp",
                "hpp"
            ],
            fix         : [],
            ignore      : [],
            ignoredFiles: [
                "interfacesMap.hpp"
            ],
            lineEnding  : "<LF>",
            maxLineLen  : 140,
            spaces      : 4
        };

        public __IgnoretestBasic() : void {
            const baseDir : string = "test/resource/data/Io/Oidis/XCppLint/AutoFormat/" +
                "DirectoryLint/";

            const files : string[] = this.Expand([
                baseDir + "/**/*.hpp",
                baseDir + "/**/*.cpp"
            ]);

            const linter : Linter = new Linter();

            files.forEach(($filePath : string) : void => {
                const fileData : string = this.readFileAbsolute($filePath);

                linter.Run(fileData, $filePath, this.config);

                if (linter.getErrors().length !== 0) {
                    // console.log($filePath);
                    linter.getErrors().forEach(($error : LintError) : void => {
                        // console.log("->[" + $error.getLine() + "] " + $error.getMessage());
                    });
                }

                linter.CleanErrors();
            });

            this.initSendBox();
        }
    }
}
